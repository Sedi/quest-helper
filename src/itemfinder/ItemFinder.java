package itemfinder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Sedi
 */
public class ItemFinder {

    public static final File mainFolder = new File("./src/itemfinder/weapons");
    public final LinkedList<Quest> quests;

    public ItemFinder() {
        quests = new LinkedList<>();
        File dir = new File("reports");
        dir.mkdir();
    }

    public void getFiles(File f) {
        File files[];
        if (f.isFile()) {
            System.out.println(f.getAbsolutePath());
        } else {
            files = f.listFiles();
            for (File file : files) {
                getFiles(file);
                try {
                    Scanner sc = new Scanner(file);
                    String line;
                    Quest q = new Quest();
                    line = sc.nextLine();
                    q.setName(line);
                    while (sc.hasNextLine()) {
                        line = sc.nextLine();
                        String[] split = line.split(", ");
                        q.addQuantity(Integer.parseInt(split[0]));
                        q.addItem(new Item(Integer.parseInt(split[2]), split[1], false));
                        //System.out.println(line);
                    }
                    quests.add(q);
                } catch (FileNotFoundException e) {
                    System.out.println("Wooopsie, something went totally wrong...");
                }
            }
        }
    }

    public LinkedList<String> search(int number) {
        LinkedList<String> list = new LinkedList<>();
        for (int i = 0; i < quests.size(); i++) {
            for (int j = 0; j < quests.get(i).getItems().size(); j++) {
                if (quests.get(i).getItems().get(j).getID() == number) {
                    list.add(quests.get(i).getQuantity().get(j) + "x for " + quests.get(i).getName());
                }
            }
        }
        return list;
    }

    public String idToName(int number) {
        for (int i = 0; i < quests.size(); i++) {
            for (int j = 0; j < quests.get(i).getItems().size(); j++) {
                if (quests.get(i).getItems().get(j).getID() == number) {
                    return quests.get(i).getItems().get(j).getName();
                }
            }
        }
        return "That item doesn't exist.";
    }

    public LinkedList<String> search(String name) {
        LinkedList<String> list = new LinkedList<>();
        for (int i = 0; i < quests.size(); i++) {
            for (int j = 0; j < quests.get(i).getItems().size(); j++) {
                if (quests.get(i).getItems().get(j).getName() == null ? name == null : quests.get(i).getItems().get(j).getName().equals(name)) {
                    list.add(quests.get(i).getQuantity().get(j) + "x for " + quests.get(i).getName());
                }
            }
        }
        return list;
    }

    public void openFile(String path) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process process;
            process = runtime.exec("C:\\Windows\\notepad.exe " + "reports\\" + path);
        } catch (IOException ex) {
        }
    }

    public void createFile(String path, LinkedList<String> list, String name) {
        try {
            FileWriter fstream = new FileWriter("reports\\" + path);
            try (BufferedWriter out = new BufferedWriter(fstream)) {
                out.write(name);
                out.newLine();
                for (int x = 0; x < list.size(); x++) {
                    out.write(list.get(x));
                    out.newLine();
                }
                out.close();
            }
        } catch (IOException e) {
        }
    }
}
