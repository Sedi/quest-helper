package itemfinder;

import java.util.LinkedList;

/**
 *
 * @author Sedi
 */
public class Quest {

    private String name;
    private LinkedList<Item> items;
    private LinkedList<Integer> quantity;

    public Quest() {
        this.name = "";
        this.items = new LinkedList<>();
        this.quantity = new LinkedList<>();
    }

    public Quest(String name, LinkedList<Item> items, LinkedList<Integer> quantity) {
        this.name = name;
        this.items = items;
        this.quantity = quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public LinkedList<Item> getItems() {
        return items;
    }

    public void setItems(LinkedList<Item> items) {
        this.items = items;
    }

    public LinkedList<Integer> getQuantity() {
        return quantity;
    }

    public void setQuantity(LinkedList<Integer> quantity) {
        this.quantity = quantity;
    }

    public void addItem(Item i) {
        items.add(i);
    }

    public void addQuantity(int i) {
        quantity.add(i);
    }

    public String tableToString() {
        String result = "";
        for (int i = 0; i < items.size(); i++) {
            result += quantity.get(i) + "x " + items.get(i) + "";
        }
        return result;
    }

    @Override
    public String toString() {
        return name + ": \n" + tableToString();
    }
}
