package itemfinder;

/**
 *
 * @author Sedi
 */
class Monster {

    private int mob_id;
    private String name;

    public Monster() {
        mob_id = 0;
        name = "";
    }

    public Monster(int mob_id, String name) {
        this.mob_id = mob_id;
        this.name = name;
    }

    public void setID(int mob_id) {
        this.mob_id = mob_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return mob_id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return mob_id + ": " + name;
    }
}
