package itemfinder;

/**
 *
 * @author Sedi
 */
public class Item {

    private int item_id;
    private String name;
    private boolean buyable;
    private Monster drop[];

    public Item() {
        item_id = 0;
        name = "";
        buyable = false;
        drop = null;
    }

    public Item(int item_id, String name, boolean buyable) {
        this.item_id = item_id;
        this.name = name;
        this.buyable = buyable;
        this.drop = null;
    }

    /**
     *
     * @param item_id
     * @param name
     * @param buyable
     * @param drop
     */
    public Item(int item_id, String name, boolean buyable, Monster drop[]) {
        this.item_id = item_id;
        this.name = name;
        this.buyable = buyable;
        this.drop = drop;
    }

    public void setID(int id) {
        this.item_id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBuyable(boolean buyable) {
        this.buyable = buyable;
    }

    public void setDrop(Monster drop[]) {
        this.drop = drop;
    }

    public int getID() {
        return item_id;
    }

    public String getName() {
        return name;
    }

    public boolean getBuyable() {
        return buyable;
    }

    public Monster[] getDrop() {
        return drop;
    }

    public String tableToString() {
        String result = "";
        for (Monster drop1 : drop) {
            result += drop1 + "\n";
        }
        return result;
    }

    @Override
    public String toString() {
        if (drop != null) {
            return name + "(" + item_id + ")" + ". Dropped from: \n" + tableToString();
        } else {
            return name + "(" + item_id + ").\n";
        }
    }
}
