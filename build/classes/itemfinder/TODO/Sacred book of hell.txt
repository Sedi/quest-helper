100x frill (dropped by frilldora, driller)
150x fine sand (sleeper, sandman)
010x loki's whisper (loli ruri, mysteltainn)
500x oil paper (karakasa, dumpling child)
050x solid peach (enchanted peach tree)
050x dry sand (mi gao in louyang field/dungeon)
050x foolishness of the blind (hode)
001x alarm mask (made in bottom left building in Aldebaran, 2nd floor: 3k clock hands and 1 mr. scream for it [note: remove your maya purple before to talk to the npc]);
001x x-shaped hairpin (made in central area in Geffen: 400 ectoplasms, 1 stellar for it);
001x lazy smokie hat (made in south-east corner in Morroc: 1k acorn, 100 sea-otter fur, 10 racoon leaf for it; you will also need a claw of desert wolf to 'activate' the npc).
001x baphomet card;
001x doppelganger card;
001x zerom card;
001x berzebub card.